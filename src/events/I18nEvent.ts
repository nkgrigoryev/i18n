export default class I18nEvent extends CustomEvent<any> {
    public static readonly INITIALIZED_EVENT = 'INITIALIZED_EVENT';
    public static readonly CHANGE_LANGUAGE_EVENT = 'CHANGE_LANGUAGE_EVENT';

    public constructor(name: string) {
        super(name);
    }
}