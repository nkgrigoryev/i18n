import EventDispatcher from '@casino/event-dispatcher'
import I18nEvent from './events/I18nEvent';
import I18nItems from './I18nItems';
import I18nItem from "./I18nItem";
import i18next from 'i18next';
import {i18n} from 'i18next';

export default class I18n extends EventDispatcher {
    private readonly _i18n: i18n;
    private readonly _items: I18nItems;

    /**
     * @param data {Object} Example:
     *     {
     *         en: {
     *             titles: {
     *                 selectLanguage: 'Select language',
     *                 simpleExample: 'Simple example',
     *                 plural: 'Plural',
     *                 notFound: 'Not found'
     *             },
     *             title: 'Title',
     *             description: 'Description',
     *             dogs: '{{count}} dog',
     *             dogs_plural: '{{count}} dogs'
     *         },
     *         ru: {
     *             titles: {
     *                 selectLanguage: 'Выберите язык',
     *                 simpleExample: 'Простой пример',
     *                 plural: 'Множественное число',
     *                 notFound: 'Не найдено'
     *             },
     *             title: 'Заголовок',
     *             description: 'Описание',
     *             dogs_0: '{{count}} собака',
     *             dogs_1: '{{count}} собаки',
     *             dogs_2: '{{count}} собак'
     *         }
     *     }
     * @param language {string}
     */
    public constructor(data: Object, language: string = undefined) {
        super();
        this._i18n = i18next.createInstance();
        this._items = new I18nItems(this._i18n);
        let lng = language ? language : Object.keys(data)[0];
        let resources = I18n._prepareData(data);
        this._i18n.init({
            lng: lng,
            resources: resources,
            initImmediate: false
        }).then();
    }

    /**
     * @param data {Object} Example:
     *     {
     *         en: {
     *             title: 'Title'
     *         },
     *         ru: {
     *             title: 'Заголовок'
     *         }
     *     }
     * @return {Object} Example:
     *     {
     *         en: {
     *             translation: {
     *                 title: 'Title'
     *             }
     *         },
     *         ru: {
     *             translation: {
     *                 title: 'Заголовок'
     *             }
     *         },
     *     }
     */
    private static _prepareData(data: Object) {
        let result = {};
        for (let key in data) {
            if (data.hasOwnProperty(key)) {
                result[key] = {
                    translation: data[key]
                }
            }
        }
        return result;
    }

    /**
     * @param element {Element}
     * @param parameters {Object} Example:
     *     {
     *         count: 1
     *     }
     */
    public t(element: Element, parameters: Object = {}): void {
        this._translate(element, parameters);
        this._translateChildren(element);
    }

    private _translateChildren(element: Element): void {
        let elements = element.getElementsByTagName('*');
        for (let i = 0; i < elements.length; i++) {
            this._translate(elements[i]);
        }
    }

    private _translate(element: Element, parameters: Object = {}): void {
        if (!I18nItem.needTranslateElement(element)) return;
        let item = this._items.getItem(element);
        item.t(parameters);
    }

    /**
     * @param language {string} Example:
     *     'en'
     */
    public changeLanguage(language: string): void {
        this._i18n.changeLanguage(language)
            .then(() => this._changeLanguage());
    }

    private _changeLanguage(): void {
        this._items.update();
        this.dispatchEvent(new I18nEvent(I18nEvent.CHANGE_LANGUAGE_EVENT));
    }

    /**
     * After this method element and children text does not change.
     * The method must be called before removing some element.
     * If this is not done, then unnecessary data will remain in memory.
     * @param element {Element}
     */
    public remove(element: Element): void {
        this._remove(element);
        this._removeChildren(element);
    }

    private _remove(element: Element): void {
        if (!I18nItem.needTranslateElement(element)) return;
        this._items.remove(element);
    }

    private _removeChildren(element: Element): void {
        let elements = element.getElementsByTagName('*');
        for (let i = 0; i < elements.length; i++) {
            this._remove(elements[i]);
        }
    }
}