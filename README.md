# README

## Installation
```bash
cd /path/to/project
yarn install
```

## Build demo
```bash
yarn webpack
```

## Use
```js
import '@casino/demo-css'
import './css/styles.css'
import createElementFromHTML from '@casino/create-element-from-html'
import translations from './demo/translations/translations'
import I18n from "@casino/i18n";

let html =
`<div>
    <div class="demo__item">
        <h2 class="demo__title" data-i18n="titles.selectLanguage">titles.selectLanguage</h2>
        <select class="demo__select languages-selector-js" style="margin-bottom: 1em">
            <option value="en">English</option>
            <option value="ru">Русский</option>
        </select>
    </div>
    <div class="demo__item">
        <h2 class="demo__title" data-i18n="titles.simpleExample">x</h2>
        <div class="demo__block demo__block_padding">
            <h1 data-i18n="title" class="title">x</h1>
            <p data-i18n="description" class="description">x</p>
        </div>
    </div>
    <div class="demo__item">
        <h2 class="demo__title" data-i18n="titles.plural">x</h2>
        <select class="demo__select dogs-selector-js" style="margin-bottom: 1em">
            <option>0</option>
            <option selected="selected">1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
            <option>10</option>
        </select>
        <div class="demo__block demo__block_padding">
            <p data-i18n="dogs" class="dogs-js">x</p>
        </div>
    </div>
    <div class="demo__item">
        <h2 class="demo__title" data-i18n="titles.notFound">x</h2>
        <div class="demo__block demo__block_padding">
            <p data-i18n="notFoundString">x</p>
        </div>
    </div>
    <div class="demo__item">
        <h2 class="demo__title" data-i18n="titles.removing">x</h2>
        <div class="demo__block demo__block_padding">
            <p data-i18n="justText" class="just-text-js">x</p>
        </div>
        <button class="demo__button remove-js">i18n.remove()</button> <button class="demo__button t-js">i18n.t()</button>
    </div>
</div>`;
let element = createElementFromHTML(html);

let i18n = new I18n(translations, 'en');

let languageSelector = element.querySelector('.languages-selector-js');
languageSelector.addEventListener('change', () => {
    i18n.changeLanguage(languageSelector.value);
});

let dogs = element.querySelector('.dogs-js');
let dogsSelector = element.querySelector('.dogs-selector-js');
dogsSelector.addEventListener('change', () => {
    i18n.t(dogs, {count: parseInt(dogsSelector.value)});
});
i18n.t(element);
i18n.t(dogs, {count: parseInt(dogsSelector.value)});

let justTextBlock = element.querySelector('.just-text-js');
let removeButton = element.querySelector('.remove-js');
removeButton.addEventListener('click', () => {
    i18n.remove(justTextBlock);
});
let tButton = element.querySelector('.t-js');
tButton.addEventListener('click', () => {
    i18n.t(justTextBlock);
});

document.getElementById('js').append(element);
```